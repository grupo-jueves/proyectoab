import React, { useEffect, useState } from "react";
import "./item.css";

function Item(props) {
  const tarea = props.tarea;
  const [estilo, setEstilo] = useState("");
  useEffect(() => {
    if (tarea.completed) {
      setEstilo("#90ab34");
    }
  }, [estilo]);
  //  console.log(estilo);
  return (
    <li className="ul-space">
        <span
          className={`Icon Icon-check ${props.completed}`}
          onClick={props.onComplete}
        >
          √
        </span>
        <span className={`Item-p ${tarea.completed && 'Item-p--complete'}`}>{tarea.text}</span>
        <span className="Icon Icon-delete" onClick={props.onDelete}>
          X
        </span>
    </li>
  );
}

export { Item };
