import React from "react";
import "./contador.css";

function Contador(props) {
  return (
    <h1>
      Has completado {props.v1} de {props.v2} tareas
    </h1>
  );
}

export { Contador };
