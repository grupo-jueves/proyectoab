import React from "react";
import "./buscador.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";

function Buscador({ buscandoValor, setBuscadorValor }) {
  const buscando = (event) => {
    setBuscadorValor(event.target.value);
  };

  return (
    <div className="wrap">
      <div className="search">
        <input
          type="text"
          className="searchTerm"
          id="input_text"
          placeholder="Buscar Tarea"
          value={buscandoValor}
          onChange={buscando}
        ></input>
      </div>
    </div>
  );
}

export { Buscador };
