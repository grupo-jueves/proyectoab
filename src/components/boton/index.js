import React, { useState } from "react";
import "./boton.css";
import Swal from "sweetalert2";

function Boton(data) {
  return (
    <button
      className="CreateTodoButton"
      onClick={() => {
        Swal.fire({
          title: "Agrega una tarea",
          html: `<input type="text" id="tarea" class="swal2-input" placeholder="Tarea">`,
          confirmButtonText: "Agregar",
          focusConfirm: false,
          preConfirm: () => {
            const tarea = Swal.getPopup().querySelector("#tarea").value;
            let newTarea = { text: tarea, completed: false, exist: true };
            let listaTareas = JSON.parse(localStorage.getItem("toDo"));
            listaTareas.push(newTarea);
            localStorage.setItem("toDo", JSON.stringify(listaTareas));
            data.data();
          },
        }).then((result) => {
          Swal.fire(`Tarea agregada con exito`);
        });
      }}
    >
      +
    </button>
  );
}

export { Boton };
