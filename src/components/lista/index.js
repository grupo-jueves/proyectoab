import React from "react";
import "./lista.css";
import { Item } from "../item";

function Lista(props) {
  const tareas = props.tareas;
  const completar = props.completadas;
  const borrar = props.borradas;
  return (
    <ul className="textbox">
      {tareas.map((tarea, index) => {
        if (tarea.exist) {
          return (
            <Item
              key={index}
              tarea={tarea}
              onComplete={() => completar(tarea.text)}
              onDelete={() => borrar(tarea.text)}
            />
          );
        }
      })}
    </ul>
  );
}

export { Lista };
