import "./App.css";
import React from "react";
import { Contador } from "./components/contador";
import { Boton } from "./components/boton";
import { Buscador } from "./components/buscador";
//import { Item } from "./components/item";
import { Lista } from "./components/lista";

const tareasDefault = JSON.stringify([
  { text: "Tarea #1", completed: false, exist: true },
  { text: "Tarea #2", completed: false, exist: true },
  { text: "Tarea #3", completed: false, exist: true },
]);

//estado

localStorage.setItem("toDo", tareasDefault);

function App() {
  //completar tareas
  const completarTareas = (text) => {
    const tareaIndex = tareas.findIndex((tarea) => tarea.text === text);
    const nuevaTarea = [...tareas];
    nuevaTarea[tareaIndex].completed = true;
    localStorage.setItem("toDo", JSON.stringify(nuevaTarea));
    test();
  };

  //borrar tareas
  const borrarTareas = (text) => {
    const tareaIndex = tareas.findIndex((tarea) => tarea.text === text);
    const nuevaTareas = [...tareas];
    nuevaTareas.splice(tareaIndex, 1);
    localStorage.setItem("toDo", JSON.stringify(nuevaTareas));
    test();
  };

  //contador
  let test = () => {
    setTareas(JSON.parse(localStorage.getItem("toDo")));
    setCompTareas(
      JSON.parse(localStorage.getItem("toDo")).filter((tarea) => {
        if (tarea.completed) {
          return true;
        }
      }).length
    );
  };
  const [tareas, setTareas] = React.useState(
    JSON.parse(localStorage.getItem("toDo"))
  );

  //Buscar tareas
  const [buscandoValor, setBuscadorValor] = React.useState("");

  let buscarTareas = [];

  if (!buscandoValor.length >= 1) {
    buscarTareas = tareas;
  } else {
    buscarTareas = tareas.filter((tareas) => {
      const tareaText = tareas.text.toLowerCase();
      const buscarText = buscandoValor.toLowerCase();
      return tareaText.includes(buscarText);
    });
  }
  //console.log("resultado..", buscarTareas);

  const [completaTareas, setCompTareas] = React.useState(
    tareas.filter((tarea) => {
      if (tarea.completed) {
        return true;
      }
    }).length
  );
  //conseguir el total
  const totalTareas = tareas.length;
  // console.log(completaTareas);
  return (
    <div className="box">
      <React.Fragment>
        <Buscador
          buscandoValor={buscandoValor}
          setBuscadorValor={setBuscadorValor}
        />
        <Contador v1={completaTareas} v2={totalTareas} />
        <Lista
          tareas={buscarTareas}
          data={test}
          completadas={completarTareas}
          borradas={borrarTareas}
        />
        <br />
        <Boton data={test} />
      </React.Fragment>
    </div>
  );
}

export default App;
