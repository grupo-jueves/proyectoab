const CACHE_ESTATICO = "static-v1";
const CACHE_DINAMICO = "dinamic-v1";
const CACHE_INMUTABLE = "inmutable-v1";

const APP_SHELL = [
  "/",
  "/index.html",
  "/js/app.js",
  "/manifest.json",
  "/sw.js",
];

const APP_SHELL_INMUTABLE = [];

function limpiarCache(cacheName, numeroItem) {
  //Agregar función debajo del put en dynamic  -> limpiarCache(CACHE_DYNAMIC,5)
  caches.open(cacheName).then((cache) => {
    return cache.keys().then((keys) => {
      if (keys.length > numeroItem) {
        cache.delete(keys[0]).then(limpiarCache(cacheName, numeroItem));
      }
    });
  });
}

self.addEventListener("install", (event) => {
  const cacheStatico = caches.open(CACHE_ESTATICO).then((cache) => {
    return cache.addAll(APP_SHELL);
  });
  const cacheInmutable = caches.open(CACHE_INMUTABLE).then((cache) => {
    return cache.addAll(APP_SHELL_INMUTABLE);
  });
  event.waitUntil(Promise.all([cacheStatico, cacheInmutable]));
});

self.addEventListener("activate", function(event) {
  event.waitUntil(self.clients.claim());
});

//Cache dinámico

self.addEventListener("fetch", (event) => {
  const respuesta = caches.match(event.request).then((res) => {
    if (res) return res;
    //No existe- ir a la web
    console.log("No existe", event.request.url);

    return fetch(event.request).then((newResp) => {
      caches.open(CACHE_DINAMICO).then((cache) => {
        cache.put(event.request, newResp);
        limpiarCache(CACHE_DINAMICO, 5);
      });

      return newResp.clone();
    });
  });
  event.respondWith(respuesta);
});
